#!/bin/sh

INDIR=$1

for logfile in `find $INDIR ! -name \*.xml -name job_\*`; do
	cp $logfile $logfile.log #prevents stream from thinking it is gzip...
	java -jar ../target/streams-mapred-streamruntime-0.0.2-SNAPSHOT.jar logAnalyser.xml -Dinfile=$logfile.log
	rm $logfile.log
done