/**
 * 
 */
package stream.test;

import java.util.Random;
import java.util.UUID;

import stream.Data;
import stream.data.DataFactory;
import stream.io.AbstractStream;

/**
 * @author chris
 * 
 */
public class RandomStream extends AbstractStream {

	Random random = new Random();
	String[] keys = "A,B,C,D,E,F,G,H".split(",");

	/**
	 * @see stream.io.AbstractStream#readNext()
	 */
	@Override
	public Data readNext() throws Exception {
		Data item = DataFactory.create();
		item.put("@id", UUID.randomUUID().toString());

		for (String key : keys) {
			item.put(key, random.nextDouble());
		}

		int[] data = new int[1024];
		for (int i = 0; i < data.length; i++) {
			data[i] = i;
		}
		item.put("randomBytes", data);
		return item;
	}
}