/**
 * 
 */
package stream.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;
import stream.data.DataFactory;
import stream.util.Variables;
import stream.util.ByteSize;
import streams.hdfs.CopyToHdfs.Block;
import streams.hdfs.HdfsUploadThread;
import streams.mapred.io.writer.DataWriter;
import streams.util.CountingOutputStream;

/**
 * @author chris
 * 
 */
public class HdfsWriter extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(HdfsWriter.class);

	/* The list of blocks waiting for upload */
	final LinkedBlockingQueue<Block> blocks = new LinkedBlockingQueue<Block>();

	/* The blockSize of the output blocks */
	ByteSize blockSize = new ByteSize(64 * ByteSize.MB);

	final Variables vars = new Variables();
	DataWriter currentWriter;
	CountingOutputStream currentStream;
	OutputStream outputStream;
	File currentBlock;
	Long size = 0L;
	Long items = 0L;
	Long totalItemSize = 0L;
	Long totalItems = 0L;

	Integer parts = 0;
	String url;
	String outputPath = "";

	String pattern = "part-%{part}";
	String outputPattern = pattern;

	Integer uploader = 1;
	final List<HdfsUploadThread> uploadThreads = new ArrayList<HdfsUploadThread>();
	String compression = "";

	/**
	 * @see stream.AbstractProcessor#init(stream.ProcessContext)
	 */
	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);

		if (url != null) {

			SourceURL su = new SourceURL(url);
			outputPath = su.getPath();
			log.info("Output path is '{}'", outputPath);
			while (outputPath.endsWith("/")) {
				outputPath = outputPath.substring(0, outputPath.length() - 1);
			}
			log.info("final outputPath is '{}'", outputPath);

			for (int i = 0; i < uploader; i++) {
				HdfsUploadThread uploadThread = new HdfsUploadThread(url,
						blocks);
				uploadThread.setDaemon(true);
				log.info("Starting fake-block-handler...");
				uploadThread.start();
				uploadThreads.add(uploadThread);
			}
			outputPattern = "/" + pattern;
			while (outputPattern.indexOf("//") > 0)
				outputPattern.replaceAll("//", "/");

			log.info("output pattern is: '{}'", outputPattern);
		}

	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		try {
			if (currentWriter == null || currentBlock == null) {
				currentBlock = startBlock();
			}

			int before = currentStream.getCount();
			currentWriter.write(input, outputStream);
			int size = currentStream.getCount();
			totalItems++;

			log.debug(
					"Item incrementing block by {} bytes, current size is: {}",
					(size - before), size);
			totalItemSize += (size - before);

			Double avgSize = totalItemSize.doubleValue()
					/ totalItems.doubleValue();
			log.debug("Average size of an item is: {}", avgSize);

			if (currentStream.getCount() + 1.0 * avgSize > blockSize.getBytes()) {
				log.debug("Next event might exceed blockSize... closing current block and starting a new one.");
				File closed = closeBlock();

				Variables vars = new Variables();
				vars.set("uuid", UUID.randomUUID().toString());
				vars.set("part", String.format("%010d", parts));
				vars.set("ext", "kryo");
				if ("deflate".equalsIgnoreCase(compression)) {
					vars.set("ext", "kryo.deflate");
				}
				if ("gzip".equalsIgnoreCase(compression)) {
					vars.set("ext", "kryo.gz");
				}

				String outFile = expand(outputPattern, vars, input);

				Block block = new Block(closed, outFile);
				log.info("Adding block {} (destination is: {})",
						block.location, block.destination);
				blocks.add(block);
				parts++;

				currentBlock = startBlock();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return input;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stream.AbstractProcessor#finish()
	 */
	@Override
	public void finish() throws Exception {
		super.finish();

		File closed = closeBlock();

		Variables vars = new Variables();
		vars.set("uuid", UUID.randomUUID().toString());
		vars.set("part", String.format("%010d", parts));
		vars.set("ext", "kryo.gz");

		String outFile = expand(outputPattern, vars, DataFactory.create());

		Block block = new Block(closed, outFile);
		log.info("Adding block {} (destination is: {})", block.location,
				block.destination);
		blocks.add(block);
		parts++;

		while (!uploadThreads.isEmpty()) {
			log.info("{} upload threads running, {} blocks pending...",
					uploadThreads.size(), blocks.size());
			try {
				HdfsUploadThread t = uploadThreads.get(0);
				t.join();
				log.info("Uploader {} finished...", t);
				uploadThreads.remove(t);
			} catch (Exception e) {
			}
		}
	}

	protected File startBlock() throws Exception {

		String ext = compression;
		if (!ext.isEmpty())
			ext = "." + ext;

		currentBlock = new File("/tmp/"
				+ File.createTempFile("hdfs-writer", ".block-" + parts)
						.getName());
		currentBlock = new File("/tmp/hdfs-writer.block-" + parts + ext);
		log.info("Starting new block in file {}", currentBlock);
		currentStream = new CountingOutputStream(new FileOutputStream(
				currentBlock));
		// outputStream = new GZIPOutputStream(currentStream);
		outputStream = currentStream;

		if ("deflate".equalsIgnoreCase(compression)) {
			outputStream = new DeflaterOutputStream(currentStream);
			log.info("Using deflater outputstream...");
		}

		if ("gzip".equalsIgnoreCase(compression)) {
			outputStream = new GZIPOutputStream(currentStream);
			log.info("Using gzip outputstream...");
		}

		log.info("Created block outputstream {}", outputStream);
		currentWriter = createWriter(outputStream);
		return currentBlock;
	}

	protected File closeBlock() throws Exception {
		log.debug("Closing current block in file {}", currentBlock);
		currentWriter.close(outputStream);
		return currentBlock;
	}

	protected DataWriter createWriter(OutputStream out) {
		DataWriter writer = new streams.mapred.io.writer.KryoWriter();
		writer.open(out);
		return writer;
	}

	/**
	 * @return the blockSize
	 */
	public ByteSize getBlockSize() {
		return blockSize;
	}

	/**
	 * @param blockSize
	 *            the blockSize to set
	 */
	public void setBlockSize(ByteSize blockSize) {
		this.blockSize = blockSize;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the pattern
	 */
	public String getPattern() {
		return pattern;
	}

	/**
	 * @param pattern
	 *            the pattern to set
	 */
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public static String expand(String str, Variables vars, Data item) {
		int i = 0;
		boolean started = false;
		boolean prestart = false;
		StringBuilder sb = new StringBuilder();
		StringBuilder macro = null;

		while (i < str.length()) {
			if (str.charAt(i) == '%')
				prestart = true;

			else if (str.charAt(i) == '{' && prestart) {
				macro = new StringBuilder();
				started = true;
				prestart = false;
			} else if (started) {
				if (str.charAt(i) == '}') {
					started = false;
					Object o = null;
					String s = macro.toString();
					if (s.startsWith("data.")) {
						o = item.get(s.substring(5));
					} else if (vars != null)
						o = vars.get(s);
					if (o != null)
						sb.append(o.toString());
				} else
					macro.append(str.charAt(i));
			} else
				sb.append(str.charAt(i));
			i++;
		}

		return sb.toString();
	}

	/**
	 * @return the uploader
	 */
	public Integer getUploadThreads() {
		return uploader;
	}

	/**
	 * @param uploader
	 *            the uploader to set
	 */
	public void setUploadThreads(Integer uploader) {
		if (uploader > 0) {
			this.uploader = uploader;
		}
	}

	/**
	 * @return the compression
	 */
	public String getCompression() {
		return compression;
	}

	/**
	 * @param compression
	 *            the compression to set
	 */
	public void setCompression(String compression) {
		this.compression = compression;
	}
}