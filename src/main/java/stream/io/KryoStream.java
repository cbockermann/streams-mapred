package stream.io;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.DefaultCodec;

import stream.Data;
import stream.data.DataFactory;
import stream.data.DataImpl;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.serializers.FieldSerializer;

public class KryoStream extends AbstractStream {

	private Input input;
	private Kryo kryo = new Kryo();

	public KryoStream() {
		kryo.register(Data.class, new FieldSerializer<Data>(kryo, Data.class) {
			@Override
			public Data create(Kryo kryo, Input input, Class<Data> type) {
				return DataFactory.create();
			}
		}, 123);
	}

	public KryoStream(File file) throws FileNotFoundException {
		this(new BufferedInputStream(new FileInputStream(file)));
	}

	public KryoStream(SourceURL url) throws IOException {
		this();

		if (url.getFile().endsWith(".deflate")) {
			CompressionCodec inputCodec = new DefaultCodec();
			Configuration conf = new Configuration();
			((DefaultCodec) inputCodec).setConf(conf);
			input = new Input(inputCodec.createInputStream(url.openStream()));
		} else {
			input = new Input(url.openStream());
		}
	}

	public KryoStream(InputStream stream) {
		this();
		input = new Input(stream);
	}

	@Override
	public Data readNext() throws Exception {
		try {
			Object object = kryo.readObject(input, DataImpl.class);

			if (Data.class.isAssignableFrom(object.getClass())) {
				return (Data) object;
			}
		} catch (KryoException e) {
			if ("Buffer underflow.".equals(e.getMessage())) {
				// Do nothing. This means "end of stream".
			} else {
				throw e;
			}
		}

		return null;
	}

	public static void main(String[] args) throws Exception {
		// String input =
		// "/Users/niklaswulf/Downloads/_user_niklaswulf_enwiki-latest-pages-short_enwiki-latest-pages-short.xml-part0000000000";
		String input = "/Users/niklaswulf/Downloads/enwiki-latest-pages-short.xml.kryo";

		Stream stream = new KryoStream(new BufferedInputStream(
				new FileInputStream(input)));
		stream.init();

		Data item = stream.read();
		while (item != null) {
			System.out.println(item);
			item = stream.read();
		}
	}
}
