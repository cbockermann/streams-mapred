package streams.hdfs;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HdfsUploader implements Runnable {
	static Logger log = LoggerFactory.getLogger(HdfsUploader.class);

	private String fileName;
	private String destination;
	private int blockSize = 128 * 1024 * 1024;

	public HdfsUploader(String fileName, String destination) {
		this.fileName = fileName;
		this.destination = destination;
	}

	@Override
	public void run() {
		try {
			Configuration configuration = new Configuration();
			configuration.set("fs.defaultfs", destination);
			configuration.set("fs.default.name", destination);
			configuration.setInt("dfs.blocksize", blockSize);
			configuration.setInt("dfs.block.size", blockSize);

			FileSystem fs = FileSystem.get(configuration);

			Path file = new Path(fileName);
			Path dst = new Path(destination);

			log.info("Uploading '{}' to '{}'.", fileName, destination);

			fs.mkdirs(dst);
			fs.moveFromLocalFile(file, dst);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int getBlockSize() {
		return blockSize;
	}

	public void setBlockSize(int blockSize) {
		this.blockSize = blockSize;
	}
}
