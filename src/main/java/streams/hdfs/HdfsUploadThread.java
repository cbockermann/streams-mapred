/**
 * 
 */
package streams.hdfs;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.io.SourceURL;
import streams.hdfs.CopyToHdfs.Block;

/**
 * @author chris
 * 
 */
public class HdfsUploadThread extends Thread implements BlockHandler {

	static Logger log = LoggerFactory.getLogger(HdfsUploadThread.class);

	final Object lock = new Object();
	File gzipCommand = new File("/usr/bin/gzip");

	private boolean running = false;
	private String hdfsUrl = "hdfs://hadoop.virtual:8020";
	private String remotePath = "/";
	private boolean delete = true;
	private FileSystem fs;
	private final LinkedBlockingQueue<Block> blocks;
	boolean runGzip = false;

	public HdfsUploadThread(String hdfsUrl, LinkedBlockingQueue<Block> blocks)
			throws IOException {
		this.blocks = blocks;
		this.hdfsUrl = hdfsUrl;
		Configuration config = new Configuration();
		config.set("fs.default.name", hdfsUrl);
		log.info("Basic HDFS url is: {}", hdfsUrl);

		try {
			remotePath = (new SourceURL(hdfsUrl).getPath());
			if (!remotePath.startsWith("/"))
				remotePath = "/" + remotePath;

			log.info("Remote path is: '{}'", remotePath);
		} catch (Exception e) {
			e.printStackTrace();
		}

		fs = FileSystem.get(config);
	}

	public boolean deletesLocalFile() {
		return delete;
	}

	public void setDeleteLocalFile(boolean delete) {
		this.delete = delete;
	}

	public String getHdfsUrl() {
		return hdfsUrl;
	}

	public void setHdfsUrl(String hdfs) {
		this.hdfsUrl = hdfs;

		Configuration config = new Configuration();
		config.set("fs.default.name", hdfs);
		try {
			fs = FileSystem.get(config);
		} catch (IOException e) {
			log.error("Failed to setup HDFS URL: {}", e.getMessage());
			if (log.isDebugEnabled())
				e.printStackTrace();
		}
	}

	public String getRemotePath() {
		return remotePath;
	}

	public void setRemotePath(String path) {
		this.remotePath = path;
	}

	public void run() {
		running = true;
		while (running) {
			try {
				Block block = blocks.take();
				if (block != null) {
					process(block);
				}
			} catch (IOException ie) {
				ie.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
				if (blocks.isEmpty() && !running) {
					break;
				}
			}
		}

		try {
			log.info("Closing HDFS connection...");
			fs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void process(Block block) throws IOException {
		log.info("Processing block {}, remotePath is '{}'", block, remotePath);
		Path src = new Path(block.location.getAbsolutePath());
		Path dst = new Path(remotePath + "/" + block.destination);

		log.info("runGzip? {}, gzip executable? {}", runGzip,
				gzipCommand.canExecute());

		if (this.runGzip && gzipCommand.canExecute()) {

			File gzipped = new File(block.location.getAbsolutePath() + ".gz");

			Process gz = Runtime.getRuntime().exec(
					new String[] { gzipCommand.getAbsolutePath(),
							block.location.getAbsolutePath() });

			try {
				int ret = gz.waitFor();
				log.info("gzip process finished with code {}", ret);
				fs.copyFromLocalFile(true, new Path(gzipped.getAbsolutePath()),
						new Path(remotePath + "/" + block.destination + ".gz"));
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			log.info("Copying from {} to {}", src, dst);
			fs.copyFromLocalFile(delete, src, dst);
		}
	}

	/**
	 * @see streams.hdfs.BlockHandler#handle(java.io.File, int, java.io.File)
	 */
	public void handle(File block, String destination) {
		log.info("Handling block {}", block);
		final Block b = new Block(block, destination);

		synchronized (lock) {

			log.info("Enqueuing new block...");
			log.info("    block {} will be uploaded as {}",
					b.location.getAbsolutePath(), b.destination);
			blocks.add(b);

			if (!running) {
				log.info("Thread was not running, yet, starting it now... ");
				start();
			}
		}
	}

	/**
	 * @see streams.hdfs.BlockHandler#close()
	 */
	public void close() {
		running = false;
	}
}