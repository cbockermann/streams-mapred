/**
 * 
 */
package streams.hdfs;

import java.io.File;

/**
 * @author chris
 * 
 */
public interface BlockHandler {

	public void handle(File block, String destination) throws Exception;

	public void close();
}
