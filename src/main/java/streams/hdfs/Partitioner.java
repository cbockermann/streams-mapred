/**
 * 
 */
package streams.hdfs;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.hadoop.io.compress.CodecPool;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.Compressor;
import org.apache.hadoop.io.compress.GzipCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.Processor;
import stream.ProcessorList;
import stream.data.DataFactory;
import stream.io.Stream;
import stream.runtime.ProcessContextImpl;
import stream.util.ByteSize;
import streams.mapred.io.writer.DataWriter;
import streams.mapred.io.writer.KryoWriter;
import streams.util.CountingOutputStream;

/**
 * @author chris
 * 
 */
public class Partitioner {

	static Logger log = LoggerFactory.getLogger(Partitioner.class);

	private final List<BlockHandler> blockHandlers = new ArrayList<BlockHandler>();
	private CompressionCodec compressionCodec = new GzipCodec();
	private int items = 0;
	private long blockStart = 0L;
	private int blockSize = 64 * 1024 * 1024; // 64 MB
	private File tempDirectory = new File("/tmp");
	private float sizeSecurityFactor = 1.5f;
	private float sizeAppriximationAlpha = 0.2f;
	private Class<? extends DataWriter> writerClass = KryoWriter.class;

	private static final String INTERMEDIATE_FILENAME = "intermediate-part";
	private static final String PART_FILENAME = "-part";
	protected final ProcessorList processors = new ProcessorList();

	protected final ProcessorList statistics = new ProcessorList();
	protected boolean statsInitialized = false;

	public Partitioner() {
		if (!tempDirectory.isDirectory())
			tempDirectory.mkdirs();
	}

	public void addBlockHandler(BlockHandler handler) {
		this.blockHandlers.add(handler);
	}

	public void addProcessor(Processor processor) {
		processors.add(processor);
	}

	public void addStatisticListener(Processor processor) {
		statistics.add(processor);
	}

	public Class<? extends DataWriter> getWriterClass() {
		return writerClass;
	}

	public void setWriterClass(Class<? extends DataWriter> writerClass) {
		this.writerClass = writerClass;
	}

	public int getBlockSize() {
		return blockSize;
	}

	public void setBlockSize(int blockSize) {
		this.blockSize = blockSize;
	}

	public CompressionCodec getCompressionCodec() {
		return compressionCodec;
	}

	public void setCompressionCodec(CompressionCodec compressionCodec) {
		this.compressionCodec = compressionCodec;
	}

	protected Data process(Data item) {
		if (item == null)
			return null;

		return processors.process(item);
	}

	public void splitStream(Stream stream, String sourceName) throws Exception {
		int part = 0;
		int size = 0;

		if (!statsInitialized) {
			try {
				log.debug("Initializing statistics listener...");
				statistics.init(new ProcessContextImpl());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		File output = new File(this.tempDirectory.getAbsolutePath()
				+ File.separator + INTERMEDIATE_FILENAME);

		// Open a stream to the file, which can be used to count written
		// content
		CountingOutputStream countingStream = new CountingOutputStream(
				new FileOutputStream(output));

		// Decorate the stream with some compression
		OutputStream compressionStream = createCompressionStream(countingStream);

		Data item = process(stream.read());

		DataWriter writer = this.writerClass.newInstance();
		writer.open(compressionStream);

		// Write an item, so that we can check, if the compression adds bytes
		// when being closed
		writer.write(item, compressionStream);
		countingStream.resetCount();

		// Check if the writer would add additional data when closed
		writer.writeEndFile(compressionStream);
		writer.close(compressionStream);
		long endLength = countingStream.getAndResetCount();

		// Check if the compression would add additional data when closed
		compressionStream.close();
		long compressionEndLength = countingStream.getAndResetCount();

		// Start new streams
		countingStream = new CountingOutputStream(new FileOutputStream(output));
		compressionStream = this.createCompressionStream(countingStream);
		writer.open(compressionStream);

		long approximatedNextDataLength = 0;
		this.blockStart = System.currentTimeMillis();

		while (item != null) {
			if (size + approximatedNextDataLength * this.sizeSecurityFactor
					+ endLength + compressionEndLength < this.blockSize) {
				writer.write(item, compressionStream);

				// Actually write the item to the disk
				compressionStream.flush();

				long dataLength = countingStream.getAndResetCount();
				size += dataLength;
				items++;

				approximatedNextDataLength = this.approximateNextDataLength(
						approximatedNextDataLength, dataLength);

				item = process(stream.read());
			} else {

				// Close the stream
				writer.writeEndFile(compressionStream);
				writer.close(compressionStream);
				compressionStream.close();

				long finished = System.currentTimeMillis();

				Data stats = DataFactory.create();
				stats.put("block:sourceName", sourceName);
				stats.put("block:id", part);
				stats.put("block:items", items);
				stats.put("block:size", size);
				stats.put("block:started", blockStart);
				stats.put("block:finished", finished);
				statistics.process(stats);

				// Rename the intermediate file
				File block = output;
				String dest = this.createOutputFile(sourceName, part);
				output.renameTo(block);
				for (BlockHandler handler : blockHandlers) {
					handler.handle(block, dest);
				}

				// Start a new part
				part++;
				output = new File(this.tempDirectory.getAbsolutePath()
						+ File.separator + INTERMEDIATE_FILENAME + "-"
						+ UUID.randomUUID().toString());
				log.info("Starting new part {} in {}", part, output);

				countingStream = new CountingOutputStream(
						new BufferedOutputStream(new FileOutputStream(output)));
				compressionStream = this
						.createCompressionStream(countingStream);
				writer.open(compressionStream);
				size = 0;
				items = 0;
			}
		}

		writer.close(compressionStream);
		compressionStream.close();

		if (size > 0) {
			String dest = this.createOutputFile(sourceName, part);
			for (BlockHandler handler : this.blockHandlers) {
				handler.handle(output, dest);
			}
		}
	}

	private String createOutputFile(String sourceName, int part) {
		String name = this.tempDirectory.getAbsolutePath() + File.separator
				+ sourceName + PART_FILENAME
				+ String.format("%10s", part).replaceAll("\\s", "0");

		if (this.compressionCodec != null) {
			// defaultExtension includes a dot (e.g. ".gz")
			name += this.compressionCodec.getDefaultExtension();
		}

		return name;
	}

	private OutputStream createCompressionStream(OutputStream stream) {
		if (this.compressionCodec != null) {
			Compressor compressor = CodecPool
					.getCompressor(this.compressionCodec);

			try {
				stream = this.compressionCodec.createOutputStream(stream,
						compressor);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return stream;
	}

	private long approximateNextDataLength(long oldValue, long newValue) {
		if (oldValue == 0)
			return newValue;

		return (long) (sizeAppriximationAlpha * oldValue + (1 - sizeAppriximationAlpha)
				* newValue);
	}

	public void close() {
		for (BlockHandler handler : this.blockHandlers) {
			try {
				handler.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {

		try {
			args = new String[] { "/Users/niklaswulf/Downloads/20111126_042.fits" };

			File input = new File(args[0]);

			// Most of these values are also default values and only included
			// here to give an example

			Partitioner partitioner = new Partitioner();
			partitioner.setCompressionCodec(new GzipCodec());
			partitioner.setBlockSize(new ByteSize("64mb").getBytes());
			partitioner.setWriterClass(KryoWriter.class);

			CopyToHdfs handler = new CopyToHdfs();
			handler.setDeleteLocalFile(true);
			handler.setHdfsUrl("hdfs://192.168.128.101:8020");
			handler.setRemotePath("/FACT/uploader/" + input.getName());
			partitioner.addBlockHandler(handler);

			// I wanted to remove the fact-tools dependency.
			// This is why this example does not work anymore.
			// Stream stream = new FactEventStream(new SourceURL(input.toURI()
			// .toURL()));
			// stream.init();
			// partitioner.splitStream(stream, input.getName());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}