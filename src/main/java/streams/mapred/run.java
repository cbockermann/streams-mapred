package streams.mapred;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.ToolRunner;

public class run {
	public static void main(String[] args) throws Exception {

		stream.runtime.StreamRuntime.loadUserProperties();
		stream.runtime.StreamRuntime.setupLogging();
		System.out.println("Setting up streams-mapred job...");

		ToolRunner.run(new Configuration(), new Driver(), args);
	}
}
