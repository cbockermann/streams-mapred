/**
 * 
 */
package streams.mapred;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.apache.hadoop.mapred.JobConf;

/**
 * @author chris
 * 
 */
public class JobUtils {

	/**
	 * This method extracts the parameters of a JobConf object which match a
	 * given prefix.
	 * 
	 * @param prefix
	 * @param conf
	 * @return
	 */
	public static java.util.Map<String, String> getParametersByPrefix(
			String prefix, JobConf conf) {
		java.util.Map<String, String> params = new LinkedHashMap<String, String>();

		Iterator<Entry<String, String>> entries = conf.iterator();
		while (entries.hasNext()) {
			Entry<String, String> entry = entries.next();

			if (entry.getKey().startsWith(prefix)) {
				params.put(entry.getKey(), entry.getValue());
			}
		}
		return params;
	}

	public static java.util.Map<String, String> stripPrefixFromKeys(
			String prefix, java.util.Map<String, String> map) {
		java.util.Map<String, String> out = new LinkedHashMap<String, String>();

		for (String key : map.keySet()) {
			if (key.startsWith(prefix)) {
				out.put(key.substring(prefix.length()), map.get(key));
			} else {
				out.put(key, map.get(key));
			}
		}

		return out;
	}
}