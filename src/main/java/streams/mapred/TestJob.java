/**
 * 
 */
package streams.mapred;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

/**
 * @author chris
 * 
 */
public class TestJob {

	public static void main(String[] args) throws Exception {
		Configuration config = new Configuration();

		JobConf job = new JobConf();
		job.setJarByClass(TestJob.class);
		job.setJobName("My first job");

		FileInputFormat.setInputPaths(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		job.setMapperClass(TestJob.MyFirstMapper.class);
		job.setReducerClass(TestJob.MyFirstReducer.class);

		JobClient client = new JobClient();
		client.submitJob(job);
	}

	private static class MyFirstMapper extends
			org.apache.hadoop.mapred.MapReduceBase implements Mapper {
		public void map(LongWritable key, Text value, OutputCollector output,
				Reporter reporter) throws IOException {

		}

		/**
		 * @see org.apache.hadoop.mapred.Mapper#map(java.lang.Object,
		 *      java.lang.Object, org.apache.hadoop.mapred.OutputCollector,
		 *      org.apache.hadoop.mapred.Reporter)
		 */
		@Override
		public void map(Object arg0, Object arg1, OutputCollector arg2,
				Reporter arg3) throws IOException {
			System.out.println("Mapping " + arg0);
		}
	}

	private static class MyFirstReducer extends
			org.apache.hadoop.mapred.MapReduceBase implements Reducer {
		public void reduce(Text key, Iterator values, OutputCollector output,
				Reporter reporter) throws IOException {

		}

		/**
		 * @see org.apache.hadoop.mapred.Reducer#reduce(java.lang.Object,
		 *      java.util.Iterator, org.apache.hadoop.mapred.OutputCollector,
		 *      org.apache.hadoop.mapred.Reporter)
		 */
		@Override
		public void reduce(Object arg0, Iterator arg1, OutputCollector arg2,
				Reporter arg3) throws IOException {
			System.out.println("Reducing key " + arg0);
		}
	}
}