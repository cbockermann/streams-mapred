package streams.mapred;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import streams.mapred.io.DataWritable;

/**
 * @author niklaswulf, Christian Bockermann
 * 
 */
public class Reduce extends MapReduceBase
		implements
		Reducer<WritableComparable<?>, DataWritable, WritableComparable<?>, DataWritable> {
	static Logger log = LoggerFactory.getLogger(Reduce.class);

	public Reduce() {
		super("reducer");
	}

	@Override
	public void configure(JobConf conf) {
		super.configure(conf);

		if (processor == null) {
			log.error("This mapper has no processor.");
			return;
		}

		initMapreduce(processor);
	}

	/**
	 * @see org.apache.hadoop.mapred.Reducer#reduce(java.lang.Object,
	 *      java.util.Iterator, org.apache.hadoop.mapred.OutputCollector,
	 *      org.apache.hadoop.mapred.Reporter)
	 */
	public void reduce(
			WritableComparable<?> inputKey,
			Iterator<DataWritable> inputValues,
			OutputCollector<WritableComparable<?>, DataWritable> outputCollector,
			Reporter reporter) throws IOException {

		if (processor == null) {
			return;
		}

		try {

			log.info("Processing data for key {}.", inputKey);
			context.setOutputCollector(outputCollector);
			context.setReporter(reporter);
			context.setCurrentKey(inputKey);
			processor.init(context);
			processor.resetState();

			int count = 0;
			while (inputValues.hasNext()) {
				DataWritable dataW = inputValues.next();
				log.debug("Processing data: {}", dataW.get());
				processor.process(dataW.get());
				count++;
			}
			log.info("   {} items processed.", count);

			processor.finish();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void close() throws IOException {
		log.info("Shutting down reducer.");

		finishMapreduce(processor);
	}
}
