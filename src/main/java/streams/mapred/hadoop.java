package streams.mapred;

import java.util.List;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.util.Tool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.io.SourceURL;

/**
 * @author niklaswulf
 * 
 */
public class hadoop extends Configured implements Tool {

	static Logger log = LoggerFactory.getLogger(hadoop.class);

	public static void main(String[] args) throws Exception {
		new hadoop().run(args);
	}

	public int run(String[] args) throws Exception {

		log.info("Creating basic job-conf....");
		JobConf baseConf = new JobConf(
				new org.apache.hadoop.conf.Configuration(), hadoop.class);

		String input = "" + args[0];
		if (!input.toLowerCase().matches("^(file|http|https|classpath):.*")) {
			input = "file:" + input;
		}

		log.info("Reading jobs from {}", input);
		List<JobConf> confs = Configuration.configurateJobs(baseConf,
				new SourceURL(input));

		log.info("Configured {} jobs.", confs.size());

		if (confs.isEmpty()) {
			throw new Exception("No job definition found!");
		}

		JobConf conf = confs.get(0);
		log.info("Starting job[0] with name '{}'...",
				conf.get(Configuration.PROPERTY_JOB_ID));
		log.info("    fs.default.name:      '{}'", conf.get("fs.default.name"));
		log.info("    mapred.job.tracker:   '{}'",
				conf.get("mapred.job.tracker"));

		// JobClient.runJob(conf);
		JobClient client = new JobClient(conf);

		if ("true".equals(conf.get("job.run.local"))) {
			log.info("Calling 'runJob(conf)'");
			JobClient.runJob(conf);
		} else {
			log.info("Calling 'submitJob(conf)'");
			client.submitJob(conf);
		}
		//
		// for (JobConf conf : confs) {
		// log.info("Starting job: {}", conf.getJobName());
		//
		// // Run the job
		// RunningJob job = JobClient.runJob(conf);
		// job.waitForCompletion();
		// if (!job.isSuccessful())
		// return -1;
		// }
		//
		return 0;
	}
}
