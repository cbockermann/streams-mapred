package streams.mapred.processors;

import stream.ProcessContext;
import stream.StatefulProcessor;

public abstract class MapredProcessor implements StatefulProcessor {

	private MapReduceContext context;

	public void init(ProcessContext context) throws Exception {
		if (!(context instanceof MapReduceContext)) {
			throw new IllegalArgumentException(
					"The given context for this processor is no MapReduceContext: "
							+ context.getClass());
		}

		init((MapReduceContext) context);
	}

	public void init(MapReduceContext context) throws Exception {
		setContext(context);
	}

	public void resetState() throws Exception {
	}

	public void finish() throws Exception {
	}

	public void initMapreduce() {
	}

	public void finishMapreduce() {
	}

	public MapReduceContext getContext() {
		return context;
	}

	public void setContext(MapReduceContext context) {
		this.context = context;
	}
}