package streams.mapred.processors;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;

public class Minimum extends AbstractProcessor {

	private String key;
	private String output = "min";
	long min = Long.MAX_VALUE;

	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);
		min = Long.MAX_VALUE;
	}

	@Override
	public void resetState() throws Exception {
		super.resetState();
		min = Long.MAX_VALUE;
	}

	@Override
	public Data process(Data input) {
		if (key == null) {
			return input;
		}

		Long value = new Long(input.get(key).toString());
		min = Math.min(value, min);
		input.put(output, min);

		return input;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

}
