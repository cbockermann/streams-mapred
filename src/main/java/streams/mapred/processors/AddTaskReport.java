package streams.mapred.processors;

import java.io.IOException;

import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.TaskAttemptID;
import org.apache.hadoop.mapred.TaskReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;

public class AddTaskReport extends MapredProcessor {

	static Logger log = LoggerFactory.getLogger(AddTaskReport.class);

	private String prefix = "mapred.taskreport.";
	private String taskAttemptKey = AddTaskAttemptId.DEFAULT_KEY;

	private JobClient client;

	@Override
	public void init(MapReduceContext context) throws Exception {
		super.init(context);
		client = new JobClient(context.getConf());
	}

	@Override
	public Data process(Data item) {
		TaskAttemptID attempt = TaskAttemptID.forName((String) item
				.get(taskAttemptKey));

		try {
			boolean foundAttempt = false;
			log.info("Searching task report for {}.", attempt);
			for (TaskReport report : client.getMapTaskReports(attempt
					.getJobID())) {
				if (report.getSuccessfulTaskAttempt().equals(attempt)) {
					log.info("Found task report.", attempt);
					item.put(prefix + "finishTime", report.getFinishTime());
					item.put(prefix + "startTime", report.getStartTime());
					item.put(prefix + "progress", report.getProgress());
					item.put(prefix + "state", report.getState());
					foundAttempt = true;
					break;
				}
			}

			if (!foundAttempt) {
				log.warn("No successful attempt found for id {}.", attempt);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return item;
	}

	public String getTaskAttemptKey() {
		return taskAttemptKey;
	}

	public void setTaskAttemptKey(String taskAttemptKey) {
		this.taskAttemptKey = taskAttemptKey;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

}
