package streams.mapred.processors;

import stream.Data;

public class IncreaseCounter extends MapredProcessor {

	private String group = "streams.mapred";
	private String counter = "count";
	private int amount = 1;

	@Override
	public Data process(Data input) {
		getContext().getReporter().incrCounter(group, counter, amount);
		return input;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getCounter() {
		return counter;
	}

	public void setCounter(String counter) {
		this.counter = counter;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

}
