package streams.mapred.processors;

import stream.AbstractProcessor;
import stream.Data;

public class Sleep extends AbstractProcessor {

	private long time = 0;

	@Override
	public Data process(Data input) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}

		return input;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

}
