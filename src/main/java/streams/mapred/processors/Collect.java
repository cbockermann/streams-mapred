package streams.mapred.processors;

import java.io.IOException;
import java.util.Set;

import org.apache.hadoop.io.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.data.DataFactory;
import stream.expressions.ExpressionResolver;
import stream.util.KeyFilter;

public class Collect extends CollectorProcessor {

	static Logger log = LoggerFactory.getLogger(Collect.class);

	private String key = "@id";
	private String[] values;
	private long collectEvery = 1;

	private Data out = null;
	private long currentItem = -1;
	private final Text KEY = new Text();

	@Override
	public void init(MapReduceContext context) throws Exception {
		super.init(context);
		currentItem = -1;
	}

	@Override
	public void resetState() throws Exception {
		super.resetState();
		currentItem = -1;
	}

	@Override
	public Data process(Data input) {
		++currentItem;

		Object o = ExpressionResolver.expand(key, getContext(), input);
		KEY.set(o.toString());

		if (values == null) {
			out = input;
		} else {
			out = DataFactory.create();
			Set<String> ks = KeyFilter.select(input, values);
			for (String k : ks) {
				out.put(k, input.get(k));
			}
		}

		if (collectEvery < 0) {
			return input;
		}

		if (collectEvery == 0 && currentItem != 0) {
			return input;
		}

		if (currentItem % collectEvery == 0) {
			try {
				collect(KEY, out);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return input;
	}

	@Override
	public void finish() throws Exception {
		if (collectEvery < 0) {
			try {
				collect(KEY, out);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String[] getValues() {
		return values;
	}

	public void setValues(String[] values) {
		this.values = values;
	}

	public long getCollectEvery() {
		return collectEvery;
	}

	public void setCollectEvery(long collectEvery) {
		this.collectEvery = collectEvery;
	}
}