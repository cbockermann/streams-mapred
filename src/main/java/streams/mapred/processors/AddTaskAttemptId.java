package streams.mapred.processors;

import org.apache.hadoop.mapred.TaskAttemptID;

import stream.Data;

public class AddTaskAttemptId extends MapredProcessor {

	public static final String DEFAULT_KEY = "mapred.taskAttemptId";

	private String key = DEFAULT_KEY;

	@Override
	public Data process(Data data) {
		TaskAttemptID attempt = TaskAttemptID.forName(getContext().getConf()
				.get("mapred.task.id"));
		data.put(key, attempt.toString());
		return data;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
