package streams.mapred.processors;

import stream.Data;

public class SetStatus extends CollectorProcessor {

	private String status = "";

	@Override
	public Data process(Data input) {
		getContext().getReporter().setStatus(
				stream.expressions.ExpressionResolver.expand(status,
						getContext(), input));

		return input;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
