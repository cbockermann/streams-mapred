/**
 * 
 */
package streams.mapred.processors;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.io.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.data.DataFactory;
import stream.data.Statistics;
import stream.util.KeyFilter;
import streams.mapred.io.DataWritable;

/**
 * @author chris
 * 
 */
public class Sum extends CollectorProcessor {

	static Logger log = LoggerFactory.getLogger(Sum.class);
	String key;
	String keys;

	Serializable pivot = "";

	Map<Serializable, Statistics> stats = new HashMap<Serializable, Statistics>();

	/**
	 * @see streams.mapred.processors.CollectorProcessor#init(streams.mapred.processors.MapReduceContext)
	 */
	@Override
	public void init(MapReduceContext context) throws Exception {
		super.init(context);
		stats.clear();
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		if (key == null) {
			pivot = "";
		} else {
			if (input.containsKey(key)) {
				pivot = input.get(key);
			} else {
				log.error("Skipping item without key '{}': {}", key, input);
				return input;
			}
		}

		for (String k : KeyFilter.select(input, keys)) {

			try {
				Double value = new Double(input.get(k) + "");

				Statistics st = stats.get(pivot);
				if (st == null) {
					st = new Statistics();
					stats.put(pivot, st);
				}

				st.add(k, value);
			} catch (Exception e) {
			}
		}

		return input;
	}

	/**
	 * @see streams.mapred.processors.CollectorProcessor#finish()
	 */
	@Override
	public void finish() throws Exception {
		super.finish();

		for (Serializable group : stats.keySet()) {
			Data item = DataFactory.create();
			item.put(key, pivot);
			Statistics st = stats.get(group);
			for (String k : st.keySet()) {
				item.put(k, st.get(k));
			}
			this.getContext()
					.getOutputCollector()
					.collect(new Text(group.toString()), new DataWritable(item));
		}
	}

	/**
	 * @return the key
	 */
	public String getGroupBy() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setGroupBy(String key) {
		this.key = key;
	}

	/**
	 * @return the keys
	 */
	public String getKeys() {
		return keys;
	}

	/**
	 * @param keys
	 *            the keys to set
	 */
	public void setKeys(String keys) {
		this.keys = keys;
	}
}
