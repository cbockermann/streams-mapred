package streams.mapred.processors;

import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

import stream.Data;
import streams.mapred.io.DataWritable;

public abstract class CollectorProcessor extends MapredProcessor {
	DataWritable data = new DataWritable();

	public void collect(WritableComparable<?> key, Data value)
			throws IOException {
		data.set(value);
		getContext().getOutputCollector().collect(key, data);
	}

	public void collect(WritableComparable<?> key, DataWritable value)
			throws IOException {
		getContext().getOutputCollector().collect(key, value);
	}
}