package streams.mapred.processors;

import java.io.IOException;
import java.io.Serializable;
import java.util.Set;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.annotations.Parameter;
import stream.data.DataFactory;
import stream.util.KeyFilter;
import streams.mapred.io.DataWritable;

public class CollectByKey extends CollectorProcessor {

	static Logger log = LoggerFactory.getLogger(CollectByKey.class);

	protected String[] keys;
	private String keyName = "@id";
	private Class<?> keyClass = Text.class;
	private DataWritable data = new DataWritable();

	public Data process(Data data) {
		if (data == null) {
			return null;
		}

		this.data.set(data);
		try {
			Serializable s = data.get(keyName);
			WritableComparable<?> key = createKey(s);
			if (key == null) {
				log.error("Skipping record without a key value for '{}'",
						keyName);
				return data;
			}

			final Data out;
			if (keys == null) {
				out = data;
			} else {
				out = DataFactory.create();
				Set<String> ks = KeyFilter.select(data, keys);
				for (String k : ks) {
					out.put(k, data.get(k));
				}
			}

			log.debug("collecting item ( '{}',  '{}'  )", key, out);
			getContext().getOutputCollector().collect(key,
					new DataWritable(out));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return data;
	}

	private WritableComparable<?> createKey(Serializable key) {

		if (key == null) {
			return null;
		}

		// TODO: @nw Add more predefined key conversions.

		if (keyClass.equals(Text.class)) {
			return new Text(key.toString());
		} else if (keyClass.equals(IntWritable.class)) {
			return new IntWritable(Integer.parseInt(key.toString()));
		}

		return keyClass.asSubclass(WritableComparable.class).cast(key);
	}

	@Parameter(description = "The name of the output key for the result tuples.", required = true)
	public void setKey(String key) {
		this.keyName = key;
	}

	public String getKey() {
		return keyName;
	}

	public String getKeyName() {
		return keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	public Class<?> getKeyClass() {
		return keyClass;
	}

	public void setKeyClass(String keyClassName) {
		try {
			this.keyClass = Class.forName(keyClassName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}