package streams.mapred.processors;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import stream.ComputeGraph;
import stream.Data;
import stream.Process;
import stream.ProcessContext;
import stream.Processor;
import stream.ProcessorList;
import stream.io.BlockingQueue;
import stream.io.Source;
import stream.runtime.ContainerContext;
import stream.runtime.DefaultProcess;
import stream.runtime.DependencyInjection;
import stream.runtime.IContainer;
import stream.runtime.setup.ServiceReference;
import stream.runtime.setup.factory.ObjectFactory;
import stream.service.Service;
import stream.util.Variables;
import streams.mapred.Configuration;

public class ProcessorFactory {

	static Logger log = LoggerFactory.getLogger(Configuration.class);
	private static final ObjectFactory objectFactory = ObjectFactory
			.newInstance();

	static class DummyContainer implements IContainer {
		public ContainerContext getContext() {
			return null;
		}

		public ComputeGraph computeGraph() {
			return null;
		}

		public Set<Source> getStreams() {
			return null;
		}

		public String getName() {
			return null;
		}

		public void setName(String name) {
		}

		public void setStream(String id, Source stream) {
		}

		public List<Process> getProcesses() {
			return null;
		}

		public Variables getVariables() {
			return null;
		}

		public void dataArrived(String key, Data item) {
		}

		public List<ServiceReference> getServiceRefs() {
			return null;
		}

		public void setProcessContext(DefaultProcess process, ProcessContext ctx) {
		}

		public void registerQueue(String id, BlockingQueue queue,
				boolean externalListener) throws Exception {
		}
	}

	public static ProcessorList createProcessor(Element jobElement,
			String tagName, MapReduceContext context, Variables variables)
			throws Exception {
		DummyContainer container = new DummyContainer();
		ProcessorList processor = new ProcessorList();

		NodeList children = jobElement.getChildNodes();

		for (int i = 0; i < children.getLength(); i++) {
			Node node = children.item(i);

			if (node.getNodeType() != Node.ELEMENT_NODE)
				continue;

			Element element = (Element) node;

			if (tagName.equalsIgnoreCase(element.getNodeName())) {
				List<Processor> processors = createNestedProcessors(container,
						element, variables);
				log.info("Created {} processors.", processors.size());
				processor.getProcessors().addAll(processors);
			}
		}

		processor.init(context);
		return processor;
	}

	private static Processor createProcessor(IContainer container,
			Element child, Variables variables) throws Exception {

		java.util.Map<String, String> params = objectFactory
				.getAttributes(child);

		Object o = objectFactory.create(child, params, variables);
		if (o instanceof Processor) {

			Variables vctx = new Variables(variables);
			if (o instanceof ProcessorList) {

				NodeList children = child.getChildNodes();
				for (int i = 0; i < children.getLength(); i++) {

					Node node = children.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {

						Element element = (Element) node;
						Processor proc = createProcessor(container, element,
								variables);
						if (proc != null) {
							((ProcessorList) o).getProcessors().add(proc);
						} else {
							log.error(
									"Nested element {} is not of type 'stream.data.Processor'.",
									node.getNodeName());
						}
					}
				}
			}

			for (String key : params.keySet()) {

				Class<? extends Service> serviceClass = DependencyInjection
						.hasServiceSetter(key, o);
				if (serviceClass != null) {
					log.info(
							"Found service setter for key '{}' in processor {}.",
							key, o);

					String ref = params.get(key);
					ref = vctx.expand(ref);
					ServiceReference serviceRef = new ServiceReference(ref, o,
							key, serviceClass);
					container.getServiceRefs().add(serviceRef);
					continue;
				}
			}

			return (Processor) o;
		}

		return null;
	}

	private static List<Processor> createNestedProcessors(IContainer container,
			Element child, Variables variables) throws Exception {
		List<Processor> procs = new ArrayList<Processor>();

		NodeList pnodes = child.getChildNodes();
		for (int j = 0; j < pnodes.getLength(); j++) {

			Node cnode = pnodes.item(j);
			if (cnode.getNodeType() == Node.ELEMENT_NODE) {
				Processor p = createProcessor(container, (Element) cnode,
						variables);
				if (p != null) {
					log.info("Found processor '{}'.", p);
					procs.add(p);
				}
			}
		}
		return procs;
	}
}
