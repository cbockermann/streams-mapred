/**
 * 
 */
package streams.mapred.processors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.hadoop.io.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.data.DataFactory;
import stream.util.KeyFilter;
import streams.mapred.io.DataWritable;

/**
 * @author chris
 * 
 */
public class CollectValues extends CollectorProcessor {

	static Logger log = LoggerFactory.getLogger(CollectValues.class);
	String groupBy;
	Serializable pivot;
	String[] keys;
	final LinkedHashMap<String, ArrayList<Serializable>> values = new LinkedHashMap<String, ArrayList<Serializable>>();

	/**
	 * @see streams.mapred.processors.CollectorProcessor#init(streams.mapred.processors.MapReduceContext)
	 */
	@Override
	public void init(MapReduceContext context) throws Exception {
		super.init(context);
		values.clear();
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		if (input.containsKey(groupBy)) {
			pivot = input.get(groupBy);
		} else {
			log.warn("Skipping item without a value for '{}': {}", groupBy,
					input);
			return input;
		}

		for (String key : KeyFilter.select(input, keys)) {
			ArrayList<Serializable> vals = values.get(key);
			if (vals == null) {
				vals = new ArrayList<Serializable>();
				values.put(key, vals);
			}
			vals.add(input.get(key));
		}

		return input;
	}

	/**
	 * @see streams.mapred.processors.CollectorProcessor#finish()
	 */
	@Override
	public void finish() throws Exception {
		super.finish();

		Data output = DataFactory.create();
		output.put(groupBy, pivot);
		output.putAll(values);

		getContext().getOutputCollector().collect(new Text(pivot + ""),
				new DataWritable(output));
	}

	/**
	 * @return the groupBy
	 */
	public String getGroupBy() {
		return groupBy;
	}

	/**
	 * @param groupBy
	 *            the groupBy to set
	 */
	public void setGroupBy(String groupBy) {
		this.groupBy = groupBy;
	}

	/**
	 * @return the keys
	 */
	public String[] getKeys() {
		return keys;
	}

	/**
	 * @param keys
	 *            the keys to set
	 */
	public void setKeys(String[] keys) {
		this.keys = keys;
	}
}