package streams.mapred.processors;

import stream.AbstractProcessor;
import stream.Data;

public class AddTimestamp extends AbstractProcessor {

	private String key = "timestamp";

	@Override
	public Data process(Data data) {
		data.put(key, System.currentTimeMillis());
		return data;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
