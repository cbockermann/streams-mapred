package streams.mapred.processors;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;

public class Maximum extends AbstractProcessor {

	private String key;
	private String output = "max";
	long max = Long.MIN_VALUE;

	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);
		max = Long.MIN_VALUE;
	}

	@Override
	public void resetState() throws Exception {
		super.resetState();
		max = Long.MIN_VALUE;
	}

	@Override
	public Data process(Data input) {
		if (key == null) {
			return input;
		}

		Long value = new Long(input.get(key).toString());
		max = Math.max(value, max);
		input.put(output, max);

		return input;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

}
