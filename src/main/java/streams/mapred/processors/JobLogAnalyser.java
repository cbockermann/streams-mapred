package streams.mapred.processors;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.ProcessContext;
import stream.StatefulProcessor;

public class JobLogAnalyser implements StatefulProcessor {

	static Logger log = LoggerFactory.getLogger(JobLogAnalyser.class);

	public static final String KEY_TYPE = "type";

	private static final String KEY_JOBID = "JOBID";
	private static final String KEY_JOBNAME = "JOBNAME";
	private static final String KEY_SUBMIT_TIME = "SUBMIT_TIME";
	private static final String KEY_FINISH_TIME = "FINISH_TIME";
	private static final String KEY_LAUNCH_TIME = "LAUNCH_TIME";
	private static final String KEY_START_TIME = "START_TIME";

	private static final String TYPE_MAP_ATTEMPT = "MapAttempt";
	private static final String TYPE_REDUCE_ATTEMPT = "ReduceAttempt";
	private static final String TYPE_JOB = "Job";

	private String separator = "\t";

	private String jobName;
	private String jobId;
	private long submitTime;
	private long launchTime;
	private long finishTime;

	private String outfile = "jobs.log";

	private long mapPhaseBeginTime = Long.MAX_VALUE;
	private long mapPhaseEndTime = Long.MIN_VALUE;

	private long reducePhaseBeginTime = Long.MAX_VALUE;
	private long reducePhaseEndTime = Long.MIN_VALUE;

	@Override
	public Data process(Data data) {
		String type = (String) data.get(KEY_TYPE);

		if (TYPE_JOB.equals(type)) {
			if (data.containsKey(KEY_JOBNAME)) {
				jobName = data.get(KEY_JOBNAME).toString();
			}
			if (data.containsKey(KEY_JOBID)) {
				jobId = data.get(KEY_JOBID).toString();
			}
			if (data.containsKey(KEY_SUBMIT_TIME)) {
				submitTime = new Long(data.get(KEY_SUBMIT_TIME).toString());
			}
			if (data.containsKey(KEY_LAUNCH_TIME)) {
				launchTime = new Long(data.get(KEY_LAUNCH_TIME).toString());
			}
			if (data.containsKey(KEY_FINISH_TIME)) {
				finishTime = new Long(data.get(KEY_FINISH_TIME).toString());
			}
		}

		if (TYPE_MAP_ATTEMPT.equals(type)) {
			if (data.containsKey(KEY_START_TIME)) {
				long time = new Long(data.get(KEY_START_TIME).toString());
				if (time - launchTime > 0)
					mapPhaseBeginTime = Math.min(mapPhaseBeginTime, time);
			}
			if (data.containsKey(KEY_FINISH_TIME)) {
				long time = new Long(data.get(KEY_FINISH_TIME).toString());
				if (time - launchTime > 0)
					mapPhaseEndTime = Math.max(mapPhaseEndTime, time);
			}
		}

		if (TYPE_REDUCE_ATTEMPT.equals(type)) {
			if (data.containsKey(KEY_START_TIME)) {
				reducePhaseBeginTime = Math.min(reducePhaseBeginTime, new Long(
						data.get(KEY_START_TIME).toString()));
			}
			if (data.containsKey(KEY_FINISH_TIME)) {
				reducePhaseEndTime = Math.max(reducePhaseEndTime, new Long(data
						.get(KEY_FINISH_TIME).toString()));
			}
		}

		return data;
	}

	@Override
	public void finish() throws Exception {
		String output = jobName + separator;
		output += jobId + separator;
		output += submitTime + separator;
		output += launchTime + separator;
		output += mapPhaseBeginTime + separator;
		output += mapPhaseEndTime + separator;
		output += reducePhaseBeginTime + separator;
		output += reducePhaseEndTime + separator;
		output += finishTime;

		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new FileWriter(outfile, true)));
			out.println(output);
			out.close();
		} catch (IOException e) {
			// oh noes!
		}

		log.info(output);
	}

	@Override
	public void init(ProcessContext arg0) throws Exception {

	}

	@Override
	public void resetState() throws Exception {

	}

	public String getOutfile() {
		return outfile;
	}

	public void setOutfile(String outfile) {
		this.outfile = outfile;
	}

}
