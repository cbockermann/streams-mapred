package streams.mapred.processors;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import stream.runtime.ProcessContextImpl;
import streams.mapred.io.DataWritable;

public class MapReduceContext extends ProcessContextImpl {

	private OutputCollector<WritableComparable<?>, DataWritable> outputCollector;
	private JobConf conf;
	private Reporter reporter;
	private WritableComparable<?> currentKey;

	public MapReduceContext() {
		super();
	}

	public OutputCollector<WritableComparable<?>, DataWritable> getOutputCollector() {
		return outputCollector;
	}

	public void setOutputCollector(
			OutputCollector<WritableComparable<?>, DataWritable> outputCollector) {
		this.outputCollector = outputCollector;
	}

	public JobConf getConf() {
		return conf;
	}

	public void setConf(JobConf conf) {
		this.conf = conf;
	}

	public Reporter getReporter() {
		return reporter;
	}

	public void setReporter(Reporter reporter) {
		this.reporter = reporter;
	}

	public WritableComparable<?> getCurrentKey() {
		return currentKey;
	}

	public void setCurrentKey(WritableComparable<?> currentKey) {
		this.currentKey = currentKey;
	}
}
