package streams.mapred;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

import org.apache.hadoop.fs.FsUrlStreamHandlerFactory;
import org.apache.hadoop.mapred.JobConf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import stream.Processor;
import stream.ProcessorList;
import stream.util.Variables;
import stream.runtime.setup.handler.PropertiesHandler;
import stream.util.XMLUtils;
import streams.mapred.processors.MapReduceContext;
import streams.mapred.processors.MapredProcessor;
import streams.mapred.processors.ProcessorFactory;

public abstract class MapReduceBase {

	static Logger log = LoggerFactory.getLogger(MapReduceBase.class);

	protected MapReduceContext context = new MapReduceContext();
	protected ProcessorList processor;

	private final String tagName;

	static {
		// Now hdfs:// URLs can be used anywhere.
		URL.setURLStreamHandlerFactory(new FsUrlStreamHandlerFactory());
	}

	public MapReduceBase(String tag) {
		this.tagName = tag;
	}

	public void configure(JobConf conf) {
		log.info("Configuring job element '{}'", tagName);

		context.setConf(conf);

		try {
			String jobId = conf.get(Configuration.PROPERTY_JOB_ID);

			log.info("Starting to configure {} from xml...", tagName);
			log.info("Job-ID: {}", jobId);

			String xml = conf.get("configuration.xml");
			log.info("my XML is:\n{}", xml);
			Document doc = XMLUtils.parseDocument(xml);

			NodeList jobNodes = doc.getElementsByTagName("job");
			Element jobElement = null;
			for (int i = 0; i < jobNodes.getLength(); i++) {
				jobElement = (Element) jobNodes.item(i);

				if (jobElement.hasAttribute(Configuration.XML_ATTRIBUTE_JOB_ID)
						&& jobElement.getAttribute("id").equals(jobId)) {
					break;
				}
			}

			if (jobElement == null
					|| !"job".equalsIgnoreCase(jobElement.getNodeName())) {
				log.error("No such job ID: {}", jobId);
				log.error(doc.toString());
				throw new Exception(
						"Failed to locate job configuration for job '" + jobId
						+ "'!");
			}

			Variables variables = Configuration.variablesFromConf(conf);

			log.info("Creating processors from {}", jobElement);
			processor = (ProcessorList) ProcessorFactory.createProcessor(
					jobElement, tagName, context, variables);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void initMapreduce(ProcessorList list) {
		for (Processor p : list.getProcessors()) {
			if (p instanceof MapredProcessor) {
				((MapredProcessor) p).initMapreduce();
			} else if (p instanceof ProcessorList) {
				initMapreduce((ProcessorList) p);
			}
		}
	}

	protected void finishMapreduce(ProcessorList list) {
		for (Processor p : list.getProcessors()) {
			if (p instanceof MapredProcessor) {
				((MapredProcessor) p).finishMapreduce();
			} else if (p instanceof ProcessorList) {
				finishMapreduce((ProcessorList) p);
			}
		}
	}

}
