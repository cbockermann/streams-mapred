package streams.mapred;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobConf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import stream.io.LineStream;
import stream.io.SourceURL;
import stream.io.Stream;
import stream.runtime.setup.handler.PropertiesHandler;
import stream.util.Variables;
import stream.util.XMLUtils;
import streams.mapred.io.DataStreamInputFormat;
import streams.mapred.io.DataWritable;
import streams.mapred.io.DataWriterOutputFormat;
import streams.mapred.io.writer.DataWriter;
import streams.mapred.io.writer.JSONWriter;

/**
 * Configuration is responsible of parsing the job's XML configuration file.
 * 
 * It sets the appropriate values of the JobConf object.
 * 
 * Also all default values are defined in here.
 * 
 * @author niklaswulf
 * 
 */
public class Configuration {

	static Logger log = LoggerFactory.getLogger(Configuration.class);

	/*
	 * XML tags and attributes
	 */
	public static final String XML_TAGNAME_JOB = "job";
	public static final String XML_ATTRIBUTE_JOB_ID = "id";
	public static final String XML_TAGNAME_INPUT = "input";
	public static final String XML_ATTRIBUTE_INPUT_PATHS = "paths";
	public static final String XML_ATTRIBUTE_INPUT_CLASS = "class";
	public static final String XML_TAGNAME_OUTPUT = "output";
	public static final String XML_ATTRIBUTE_OUTPUT_PATH = "path";
	public static final String XML_ATTRIBUTE_OUTPUT_DELETEEXISTING = "deleteExisting";
	public static final String XML_TAGNAME_MAPPER = "mapper";
	public static final String XML_ATTRIBUTE_MAPPER_OUTPUTKEYCLASS = "outputKeyClass";
	public static final String XML_TAGNAME_REDUCER = "reducer";
	public static final String XML_ATTRIBUTE_REDUCER_OUTPUTKEYCLASS = "outputKeyClass";
	public static final String XML_TAGNAME_HADOOP = "hadoop";
	public static final String XML_TAGNAME_HADOOP_PROPERTY = "property";
	public static final String XML_TAGNAME_HADOOP_PROPERTY_NAME = "name";
	public static final String XML_TAGNAME_HADOOP_PROPERTY_VALUE = "value";

	/*
	 * Configuration values
	 */
	public static final String PROPERTY_STREAM_CLASS = "streams.mapred.inputstreamclass";
	public static final String PROPERTY_JOB_ID = "streams.mapred.jobid";

	public static final String PROPERTY_OUTPUT_DIR = "mapred.output.dir";

	public static final String PATH_DELIMITER = ",";

	/*
	 * Default values
	 */
	public static final Class<? extends WritableComparable<?>> DEFAULT_REDUCER_OUTPUTKEYCLASS = Text.class;
	public static final Class<? extends WritableComparable<?>> DEFAULT_MAPPER_OUTPUTKEYCLASS = Text.class;
	public static final Class<? extends Stream> DEFAULT_INPUT_STREAM_CLASS = LineStream.class;
	public static final boolean DEFAULT_DELETEEXISTINGOUTPUT = false;
	public static final java.util.Map<String, String> DEFAULT_HADOOP_CONFIGURATION;
	public static final Class<? extends DataWriter> DEFAULT_DATAWRITER_CLASS = JSONWriter.class;

	static {
		// see http://hadoop.apache.org/docs/r1.1.2/mapred-default.html for
		// possible keys and values

		java.util.Map<String, String> conf = new HashMap<String, String>();
		conf.put("mapred.job.reuse.jvm.num.tasks", "3");
		conf.put("mapred.reduce.tasks", "6");
		conf.put("mapred.job.name", "streams.mapred");
		conf.put("mapred.output.format.class",
				DataWriterOutputFormat.class.getName());

		DEFAULT_HADOOP_CONFIGURATION = Collections.unmodifiableMap(conf);
	}

	public static List<JobConf> configurateJobs(JobConf baseConf, SourceURL url)
			throws Exception {

		log.info("Configuring jobs...");
		printConf(baseConf);

		List<JobConf> jobs = new LinkedList<JobConf>();

		Variables baseVariables = variablesFromConf(baseConf);

		Document doc = XMLUtils.parseDocument(url.openStream());
		String xml = XMLUtils.toString(doc);

		Variables properties = new Variables();
		PropertiesHandler props = new PropertiesHandler();
		props.handle(null, doc, properties, null);
		log.info("Properties: {}", new HashMap<String, String>(properties));
		baseVariables.putAll(properties);

		NodeList jobNodes = doc.getElementsByTagName(XML_TAGNAME_JOB);
		log.info("Found {} job nodes.", jobNodes.getLength());

		for (int i = 0; i < jobNodes.getLength(); i++) {
			Element jobElement = (Element) jobNodes.item(i);

			if (!jobElement.hasAttribute(XML_ATTRIBUTE_JOB_ID)) {
				log.error("No '{}' attribute provided for job element!",
						XML_ATTRIBUTE_JOB_ID);
				throw new Exception("No '" + XML_ATTRIBUTE_JOB_ID
						+ "' attribute provided for job element!");
			}

			log.debug(
					"Creating new job-conf object for job element with ID '{}'",
					jobElement.getAttribute("id"));
			JobConf conf = new JobConf(baseConf, Driver.class);
			Variables confVariables = new Variables(
					baseVariables.expandAll(baseVariables));
			expandConfigurationVariables(conf, confVariables);

			conf.set(PROPERTY_JOB_ID,
					jobElement.getAttribute(XML_ATTRIBUTE_JOB_ID));

			log.info("Adding 'configuration.xml' property, holding the XML configuration...");
			log.info("XML config is:\n{}", xml);
			conf.set("configuration.xml", xml);
			configurateJob(conf, jobElement, confVariables);

			String jobName = "streams.mapred:" + url.toString() + ":";

			conf.setJobName(jobElement.getAttribute(XML_ATTRIBUTE_JOB_ID));

			jobs.add(conf);

			log.info("Configured job '{}'.", jobName);
			printConf(baseConf);
		}

		return jobs;
	}

	private static void printConf(JobConf conf) {
		log.trace("Base conf:");

		for (Entry<String, String> entry : conf) {
			log.trace("    {} = {}", entry.getKey(), entry.getValue());
		}
	}

	public static Variables variablesFromConf(JobConf conf) {
		Variables variables = new Variables();
		for (Entry<String, String> entry : conf) {
			variables.set(entry.getKey(), entry.getValue());
		}
		return variables;
	}

	private static void expandConfigurationVariables(JobConf conf,
			Variables baseVariables) {
		for (Entry<String, String> entry : conf) {
			baseVariables.set(entry.getKey(), entry.getValue());
		}

		Variables variables = new Variables(
				baseVariables.expandAll(baseVariables));
		for (Entry<String, String> entry : variables.entrySet()) {
			conf.set(entry.getKey(), entry.getValue());
		}
	}

	/**
	 * @throws ClassNotFoundException
	 * 
	 */
	private static void configurateJob(JobConf conf, Element jobElement,
			Variables variables) throws IOException,
			ParserConfigurationException, SAXException, URISyntaxException,
			ClassNotFoundException {

		// First set some default values
		for (Entry<String, String> entry : DEFAULT_HADOOP_CONFIGURATION
				.entrySet()) {
			conf.set(entry.getKey(), entry.getValue());
		}
		DataWriterOutputFormat.setWriterClass(DEFAULT_DATAWRITER_CLASS);

		// Now set the essential classes
		conf.setInputFormat(DataStreamInputFormat.class);
		conf.setOutputValueClass(DataWritable.class);
		conf.setMapperClass(Map.class);
		conf.setReducerClass(Reduce.class);

		conf.setOutputFormat(DataWriterOutputFormat.class);

		// Next parse the job's configuration
		NodeList children = jobElement.getChildNodes();

		for (int i = 0; i < children.getLength(); i++) {
			Node node = children.item(i);

			if (node.getNodeType() != Node.ELEMENT_NODE)
				continue;

			handleElement(conf, (Element) node, variables);
		}

		conf.setOutputKeyComparatorClass(FloatWritable.Comparator.class);
		conf.setOutputValueGroupingComparator(FloatWritable.Comparator.class);
	}

	private static void handleElement(JobConf conf, Element element,
			Variables variables) throws IOException, ClassNotFoundException {
		String tagName = element.getNodeName();

		if (XML_TAGNAME_INPUT.equalsIgnoreCase(tagName)) {
			handleInputElement(conf, element, variables);
		} else if (XML_TAGNAME_OUTPUT.equalsIgnoreCase(tagName)) {
			handleOutputElement(conf, element, variables);
		} else if (XML_TAGNAME_MAPPER.equalsIgnoreCase(tagName)) {
			handleMapperElement(conf, element, variables);
		} else if (XML_TAGNAME_REDUCER.equalsIgnoreCase(tagName)) {
			handleReducerElement(conf, element, variables);
		} else if (XML_TAGNAME_HADOOP.equalsIgnoreCase(tagName)) {
			handleHadoopElement(conf, element, variables);
		}
	}

	private static void handleHadoopElement(JobConf conf, Element element,
			Variables variables) {
		NodeList children = element.getChildNodes();

		for (int i = 0; i < children.getLength(); i++) {
			Node node = children.item(i);

			if (node.getNodeType() != Node.ELEMENT_NODE
					|| !XML_TAGNAME_HADOOP_PROPERTY.equalsIgnoreCase(node
							.getNodeName()))
				continue;

			Element e = (Element) node;
			NodeList names = e
					.getElementsByTagName(XML_TAGNAME_HADOOP_PROPERTY_NAME);
			if (names.getLength() != 1)
				continue;

			String key = names.item(0).getTextContent();

			NodeList values = e
					.getElementsByTagName(XML_TAGNAME_HADOOP_PROPERTY_VALUE);
			if (values.getLength() != 1)
				continue;

			String value = variables.expand(values.item(0).getTextContent());

			log.info("JobConf: {} = {}", key, value);
			conf.set(key, value);
		}
		expandConfigurationVariables(conf, variables);
	}

	private static void handleReducerElement(JobConf conf, Element element,
			Variables variables) {
		String outputClassName = variables.expand(element
				.getAttribute(XML_ATTRIBUTE_REDUCER_OUTPUTKEYCLASS));

		Class<?> outputClass = DEFAULT_REDUCER_OUTPUTKEYCLASS;
		try {
			if (outputClassName != null && !outputClassName.trim().isEmpty()) {
				outputClass = Class.forName(outputClassName);
			} else {
				log.warn(
						"No output-key-class for reducer defined, using default '{}'",
						outputClass);
			}
		} catch (ClassNotFoundException e) {
			log.warn("No such class for reducer output key: {}",
					outputClassName);
			log.warn("Using {} as a default.", DEFAULT_REDUCER_OUTPUTKEYCLASS);

			outputClass = DEFAULT_REDUCER_OUTPUTKEYCLASS;
		}

		conf.setOutputKeyClass(outputClass);
		log.info("Reducer output key class: {}", outputClass);
	}

	private static void handleMapperElement(JobConf conf, Element element,
			Variables variables) {
		String outputClassName = variables.expand(element
				.getAttribute(XML_ATTRIBUTE_MAPPER_OUTPUTKEYCLASS));

		Class<?> outputClass = DEFAULT_MAPPER_OUTPUTKEYCLASS;
		try {
			if (outputClassName != null && !outputClassName.trim().isEmpty()) {
				outputClass = Class.forName(outputClassName);
			} else {
				log.warn(
						"No output-key-class for mapper defined, using default '{}'",
						outputClass);
			}
		} catch (ClassNotFoundException e) {
			log.warn("No such class for mapper output key: {}", outputClassName);
			log.warn("Using {} as a default.", DEFAULT_REDUCER_OUTPUTKEYCLASS);

			outputClass = DEFAULT_MAPPER_OUTPUTKEYCLASS;
		}

		conf.setMapOutputKeyClass(outputClass);
		log.info("Mapper output key class: {}", outputClass);
	}

	private static void handleOutputElement(JobConf conf, Element element,
			Variables variables) throws IOException {
		String path = variables.expand(element
				.getAttribute(XML_ATTRIBUTE_OUTPUT_PATH));
		Path outputPath = new Path(path);

		boolean deleteExisting = DEFAULT_DELETEEXISTINGOUTPUT;

		if (element.hasAttribute(XML_ATTRIBUTE_OUTPUT_DELETEEXISTING)) {
			log.debug("has deleteExisting attribute.");
			deleteExisting = Boolean.parseBoolean(variables.expand(element
					.getAttribute(XML_ATTRIBUTE_OUTPUT_DELETEEXISTING)));
		}

		if (deleteExisting) {
			FileSystem fs = FileSystem.get(conf);
			fs.delete(outputPath, true);
			log.info("Deleting output directory: {}", outputPath);
		}

		String writerClass = element.getAttribute("class");
		if (writerClass != null && !writerClass.isEmpty()) {
			try {
				Class<? extends DataWriter> cl = (Class<? extends DataWriter>) Class
						.forName(writerClass);
				log.info("Setting writer output class to '{}'", cl);
				DataWriterOutputFormat.setWriterClass(cl);

				java.util.Map<String, String> params = XMLUtils
						.getAttributes(element);
				for (String key : params.keySet()) {
					String property = "output.writer." + key;
					String value = params.get(key);
					log.info("Adding job property '{}' = '{}'", property, value);
					conf.set("output.writer." + property, value);
				}

			} catch (Exception e) {
				throw new IOException("Failed to create writer class '"
						+ writerClass + "': " + e.getMessage());
			}
		}

		FileOutputFormat.setOutputPath(conf, outputPath);

		log.info("Output directory: {}", outputPath);
	}

	private static void handleInputElement(JobConf conf, Element element,
			Variables variables) throws ClassNotFoundException {
		String paths = variables.expand(element
				.getAttribute(XML_ATTRIBUTE_INPUT_PATHS));
		log.info("Input paths: '{}'", paths);
		String[] pathList = paths.split(PATH_DELIMITER);
		Path[] p = new Path[pathList.length];
		int i = 0;
		for (String s : pathList) {
			p[i] = new Path(s);
			++i;
		}

		FileInputFormat.setInputPaths(conf, p);

		String streamClassName = variables.expand(element
				.getAttribute(XML_ATTRIBUTE_INPUT_CLASS));

		Class<?> streamClass = Class.forName(streamClassName);

		log.info("Input paths: {}", paths);
		log.info("Input stream class: {}", streamClass);

		conf.setClass(PROPERTY_STREAM_CLASS, streamClass, Stream.class);
	}

}
