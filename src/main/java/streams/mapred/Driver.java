package streams.mapred;

import java.util.List;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.RunningJob;
import org.apache.hadoop.util.Tool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.io.SourceURL;

/**
 * @author niklaswulf
 * 
 */
public class Driver extends Configured implements Tool {

	static Logger log = LoggerFactory.getLogger(Driver.class);

	public int run(String[] args) throws Exception {

		log.info("Creating basic job-conf...");
		JobConf baseConf = new JobConf(getConf(), Driver.class);

		String input = "" + args[0];
		if (!input.toLowerCase().matches("^(file|http|https|classpath):.*")) {
			input = "file:" + input;
		}

		log.info("Reading jobs from {}", args[0]);
		List<JobConf> confs = Configuration.configurateJobs(baseConf,
				new SourceURL(input));

		log.info("Configured {} jobs.", confs.size());

		for (JobConf conf : confs) {
			log.info("Starting job: {}", conf.getJobName());

			// Run the job
			RunningJob job = JobClient.runJob(conf);
			job.waitForCompletion();
			if (!job.isSuccessful())
				return -1;
		}

		return 0;
	}
}
