package streams.mapred.io;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.compress.CodecPool;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.Compressor;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.RecordWriter;
import org.apache.hadoop.util.Progressable;
import org.apache.hadoop.util.ReflectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Processor;
import streams.mapred.JobUtils;
import streams.mapred.io.writer.DataWriter;
import streams.mapred.io.writer.JSONWriter;
import streams.mapred.io.writer.ProcessorWriter;

public class DataWriterOutputFormat extends
		FileOutputFormat<WritableComparable<?>, DataWritable> {
	static Logger log = LoggerFactory.getLogger(DataWriterOutputFormat.class);

	private static Class<?> writerClass = JSONWriter.class;

	public static void setWriterClass(Class<? extends DataWriter> clazz) {
		writerClass = clazz;
	}

	@SuppressWarnings("unchecked")
	@Override
	public RecordWriter<WritableComparable<?>, DataWritable> getRecordWriter(
			FileSystem ignored, JobConf conf, String name, Progressable progress)
			throws IOException {
		try {
			// DataWriter writer = writerClass.newInstance();
			Path path = getPathForCustomFile(conf, name);
			FileSystem fs = FileSystem.get(conf);
			OutputStream out = fs.create(path);

			log.info("Creating new DataWriterOutputFormat...");
			log.debug("path = " + path);
			log.debug("out = " + out);

			Map<String, String> params = JobUtils.getParametersByPrefix(
					"output.writer.", conf);
			params = JobUtils.stripPrefixFromKeys("output.writer.", params);

			DataWriter writer = null;
			try {
				log.info("Creating new data-writer instance from options: {}",
						params);

				Class<?> cl = Class.forName(params.get("output.writer.class"));

				if (cl.isAssignableFrom(DataWriter.class)) {
					Class<? extends DataWriter> dwcl = (Class<? extends DataWriter>) cl;
					log.info("Setting writer output class to '{}'", cl);
					DataWriterOutputFormat.setWriterClass(dwcl);

					writer = ReflectionUtils.newInstance(dwcl, conf);
				} else if (cl.isAssignableFrom(Processor.class)) {
					DataWriterOutputFormat
							.setWriterClass(ProcessorWriter.class);
					ProcessorWriter
							.setProcessorClass((Class<? extends Processor>) cl);

					writer = ReflectionUtils.newInstance(ProcessorWriter.class,
							conf);
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new IOException("Failed to create writer: "
						+ e.getMessage());
			}

			Class<? extends CompressionCodec> codecClass = getOutputCompressorClass(
					conf, null);
			if (codecClass != null && getCompressOutput(conf)) {
				CompressionCodec codec = ReflectionUtils.newInstance(
						codecClass, conf);
				Compressor compressor = CodecPool.getCompressor(codec);

				log.debug("codec = " + codec);
				out = codec.createOutputStream(out, compressor);
			} else {
				log.debug("no compression");
			}

			return new DataRecordWriter(out, writer);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}
