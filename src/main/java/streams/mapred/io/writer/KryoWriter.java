package streams.mapred.io.writer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import stream.Data;
import stream.data.DataFactory;
import stream.io.LineStream;
import stream.io.Stream;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.serializers.FieldSerializer;

public class KryoWriter extends DataWriter {

	Kryo kryo = new Kryo();
	Output output;

	public KryoWriter() {
		kryo.register(Data.class, new FieldSerializer<Data>(kryo, Data.class) {
			@Override
			public Data create(Kryo kryo, Input input, Class<Data> type) {
				return DataFactory.create();
			}
		}, 123);
	}

	public void open(OutputStream stream) {
		output = new Output(stream);
	}

	public void write(Data item, OutputStream stream) {
		kryo.writeObject(output, item);
		output.flush();
	}

	public void close(OutputStream stream) {
		output.close();
	}

	public static void main(String[] args) throws FileNotFoundException,
			Exception {
		String input = "/Users/niklaswulf/Downloads/enwiki-latest-pages-short.xml";
		String output = "/Users/niklaswulf/Downloads/enwiki-latest-pages-short.xml.kryo";

		Stream stream = new LineStream(new FileInputStream(input));
		stream.init();
		DataWriter writer = new KryoWriter();

		File outputFile = new File(output);
		FileOutputStream fileOutputStream = new FileOutputStream(outputFile);

		writer.open(fileOutputStream);

		Data item = stream.read();
		while (item != null) {
			writer.write(item, fileOutputStream);
			item = stream.read();
		}

		writer.close(fileOutputStream);
		fileOutputStream.close();
	}

	public void writeEndFile(OutputStream stream) {
		// NOP
	}

	@Override
	public void write(String key, Data item, OutputStream stream) {
		// overwriting write(), means this method is not used anymore
	}
}
