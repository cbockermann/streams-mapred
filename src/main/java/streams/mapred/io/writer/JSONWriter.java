package streams.mapred.io.writer;

import java.io.OutputStream;
import java.io.PrintStream;

import net.minidev.json.JSONObject;
import stream.Data;

public class JSONWriter extends DataWriter {

	public void write(Data item, OutputStream out) {
		String line = JSONObject.toJSONString(item);

		PrintStream ps = new PrintStream(out);
		ps.println(line);
	}

	public void open(OutputStream stream) {
	}

	public void close(OutputStream stream) {
	}

	public void writeEndFile(OutputStream stream) {
	}

	@Override
	public void write(String key, Data item, OutputStream stream) {
		// overwriting write(), means this method is not used anymore
	}
}
