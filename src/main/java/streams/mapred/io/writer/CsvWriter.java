package streams.mapred.io.writer;

import java.io.OutputStream;
import java.io.PrintStream;

import org.apache.commons.lang.StringUtils;

import stream.Data;

public class CsvWriter extends DataWriter {

	boolean first = true;
	char separator = ',';

	@Override
	public void open(OutputStream stream) {
	}

	@Override
	public void writeEndFile(OutputStream stream) {
	}

	@Override
	public void close(OutputStream stream) {
	}

	@Override
	public void write(String key, Data item, OutputStream stream) {
		PrintStream ps = new PrintStream(stream);

		if (first) {
			ps.println("mapred.key,"
					+ StringUtils.join(item.keySet().toArray(), separator));
			first = false;
		}

		ps.println(key + "," + StringUtils.join(item.values(), separator));
	}

}
