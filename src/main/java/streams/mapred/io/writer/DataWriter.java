package streams.mapred.io.writer;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;

import org.apache.hadoop.io.WritableComparable;

import stream.Data;
import stream.data.DataFactory;
import streams.mapred.io.DataWritable;

public abstract class DataWriter {

	public void write(WritableComparable<?> key, DataWritable value,
			OutputStream stream) throws IOException {
		Data item = DataFactory.create();
		item.put(key.toString(), value.get());
		write(item, stream);
	}

	public void write(Data item, OutputStream stream) {
		Set<String> keys = item.keySet();
		if (keys.isEmpty())
			return;

		String key = keys.iterator().next();
		Data value = (Data) item.get(key);
		write(key, value, stream);
	}

	public abstract void open(OutputStream stream);

	public abstract void write(String key, Data item, OutputStream stream);

	public abstract void writeEndFile(OutputStream stream);

	public abstract void close(OutputStream stream);
}
