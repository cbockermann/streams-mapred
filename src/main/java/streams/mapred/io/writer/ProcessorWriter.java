package streams.mapred.io.writer;

import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;

import stream.Data;
import stream.Processor;
import stream.StatefulProcessor;
import streams.mapred.processors.MapReduceContext;

public class ProcessorWriter extends DataWriter {

	private static Class<? extends Processor> processorClass;

	Processor writer;

	@Override
	public void open(OutputStream stream) {
		if (processorClass == null) {
			throw new IllegalArgumentException(
					"No processor class has been configured.");
		}

		try {
			if (writer == null) {
				writer = (Processor) processorClass.getConstructor(
						OutputStream.class).newInstance(stream);

				if (writer instanceof StatefulProcessor) {
					((StatefulProcessor) writer).init(new MapReduceContext());
				}
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void writeEndFile(OutputStream stream) {
	}

	@Override
	public void close(OutputStream stream) {
		if (writer instanceof StatefulProcessor) {
			try {
				((StatefulProcessor) writer).finish();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void write(String key, Data item, OutputStream stream) {
		item.put("mapred.key", key);
		writer.process(item);
	}

	public static Class<? extends Processor> getProcessorClass() {
		return processorClass;
	}

	public static void setProcessorClass(
			Class<? extends Processor> processorClass) {
		ProcessorWriter.processorClass = processorClass;
	}
}
