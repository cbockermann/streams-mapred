package streams.mapred.io;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import stream.Data;
import stream.data.DataFactory;
import stream.io.AbstractStream;
import stream.io.SourceURL;
import streams.mapred.processors.JobLogAnalyser;

public class JobLogStream extends AbstractStream {

	private BufferedReader reader;
	private Pattern pattern = Pattern.compile("(\\w*)=\"(.*)\"");

	public JobLogStream(SourceURL url) {
		super(url);
	}

	@Override
	public void init() throws Exception {
		super.init();
		reader = new BufferedReader(new InputStreamReader(url.openStream()));
	}

	@Override
	public Data readNext() throws Exception {
		Data item = DataFactory.create();
		String line = reader.readLine();
		if (line == null)
			return null;

		StringTokenizer tokenizer = new StringTokenizer(line);
		String token = tokenizer.nextToken();

		item.put(JobLogAnalyser.KEY_TYPE, token);

		try {
			while ((token = tokenizer.nextToken()) != null) {

				Matcher matcher = pattern.matcher(token);
				while (matcher.find()) {
					String key = matcher.group(1);
					String value = matcher.group(2);

					item.put(key, value);
				}
			}
		} catch (NoSuchElementException e) {
			return item;
		}

		return item;
	}

}
