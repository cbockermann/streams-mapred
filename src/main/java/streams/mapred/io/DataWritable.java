package streams.mapred.io;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.hadoop.io.ObjectWritable;

import stream.Data;
import stream.io.Serializer;
import stream.util.JavaSerializer;

public class DataWritable extends ObjectWritable {

	private static final Serializer serializer = new JavaSerializer();

	public DataWritable() {
		super();
	}

	public DataWritable(Data data) {
		super(data);
	}

	public Data get() {
		return (Data) super.get();
	}

	public void set(Data data) {
		super.set(data);
	}

	/**
	 * @see org.apache.hadoop.io.ObjectWritable#readFields(java.io.DataInput)
	 */
	@Override
	public void readFields(DataInput in) throws IOException {
		if (in instanceof InputStream) {
			set((Data) serializer.read((InputStream) in));
		} else {
			super.readFields(in);
		}
	}

	/**
	 * @see org.apache.hadoop.io.ObjectWritable#write(java.io.DataOutput)
	 */
	@Override
	public void write(DataOutput out) throws IOException {
		if (out instanceof OutputStream) {
			Data data = get();
			serializer.write(data, (OutputStream) out);
			set(data);
		} else {
			super.write(out);
		}
	}
}
