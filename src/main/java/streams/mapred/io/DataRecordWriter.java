package streams.mapred.io;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapred.RecordWriter;
import org.apache.hadoop.mapred.Reporter;

import streams.mapred.io.writer.DataWriter;

public class DataRecordWriter implements
		RecordWriter<WritableComparable<?>, DataWritable> {

	private OutputStream stream;
	private DataWriter writer;

	public DataRecordWriter(OutputStream stream, DataWriter writer) {
		this.stream = stream;
		this.writer = writer;
		writer.open(stream);
	}

	/**
	 * @see org.apache.hadoop.mapred.RecordWriter#close(org.apache.hadoop.mapred.Reporter)
	 */
	public void close(Reporter arg0) throws IOException {
		writer.close(stream);
		stream.close();
	}

	/**
	 * @see org.apache.hadoop.mapred.RecordWriter#write(java.lang.Object,
	 *      java.lang.Object)
	 */
	public void write(WritableComparable<?> key, DataWritable value)
			throws IOException {
		writer.write(key, value, stream);
	}
}