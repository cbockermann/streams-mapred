package streams.mapred.io;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.compress.CodecPool;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.CompressionCodecFactory;
import org.apache.hadoop.io.compress.Decompressor;
import org.apache.hadoop.mapred.FileSplit;
import org.apache.hadoop.mapred.InputSplit;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.RecordReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.data.DataFactory;
import stream.io.Stream;
import streams.mapred.Configuration;

public class DataStreamRecordReader implements
		RecordReader<IntWritable, DataWritable> {
	static Logger log = LoggerFactory.getLogger(DataStreamRecordReader.class);

	private Stream stream;
	private FileSplit split;
	private FSDataInputStream inputStream;
	private int currentId = 0;

	public DataStreamRecordReader(InputSplit inputSplit, JobConf conf)
			throws Exception {
		super();

		this.split = (FileSplit) inputSplit;

		Class<? extends Stream> streamClass = conf.getClass(
				Configuration.PROPERTY_STREAM_CLASS,
				Configuration.DEFAULT_INPUT_STREAM_CLASS, Stream.class);

		final Path file = split.getPath();
		final FileSystem fs = file.getFileSystem(conf);

		log.info("Creating new DataStreamRecordReader...");
		log.info("...file = " + file.toString());
		log.info("...streamClass = " + streamClass.getName());
		log.info("...inputSplit = " + inputSplit.toString());

		inputStream = fs.open(file);
		InputStream in = inputStream;
		inputStream.seek(split.getStart());

		CompressionCodecFactory factory = new CompressionCodecFactory(conf);
		CompressionCodec codec = factory.getCodec(file);
		if (codec != null) {
			Decompressor decompressor = CodecPool.getDecompressor(codec);

			in = codec.createInputStream(in, decompressor);
			log.info("The compression codec is " + codec);
		} else {
			log.info("There is no (or no compatible) compression. Installed codecs: "
					+ CompressionCodecFactory.getCodecClasses(conf));
		}

		Constructor<? extends Stream> constructor = streamClass
				.getDeclaredConstructor(InputStream.class);
		stream = constructor.newInstance(in);
		stream.init();
	}

	public void close() throws IOException {
		inputStream.close();
	}

	public IntWritable createKey() {
		return new IntWritable();
	}

	public DataWritable createValue() {
		return new DataWritable(DataFactory.create());
	}

	public long getPos() throws IOException {
		if (inputStream == null)
			return 0;

		return inputStream.getPos();
	}

	public float getProgress() throws IOException {
		if (inputStream == null)
			return 0;

		return ((float) (inputStream.getPos() - split.getStart()) / (float) (split
				.getLength() - split.getStart()));
	}

	public boolean next(IntWritable id, DataWritable writable)
			throws IOException {
		try {
			id.set(currentId++);
			Data next = stream.read();
			if (next == null)
				return false;

			writable.set(next);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

}
