package streams.mapred.io;

import java.io.IOException;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.InputSplit;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.RecordReader;
import org.apache.hadoop.mapred.Reporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataStreamInputFormat extends
		FileInputFormat<IntWritable, DataWritable> {

	static Logger log = LoggerFactory.getLogger(DataStreamInputFormat.class);

	public DataStreamInputFormat() {
		super();
	}

	@Override
	public RecordReader<IntWritable, DataWritable> getRecordReader(
			InputSplit inputSplit, JobConf conf, Reporter reporter)
			throws IOException {
		try {
			return new DataStreamRecordReader(inputSplit, conf);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	protected boolean isSplitable(FileSystem fs, Path fileName) {
		return false;
	}
}
