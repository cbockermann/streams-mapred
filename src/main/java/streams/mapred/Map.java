package streams.mapred;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import streams.mapred.io.DataWritable;

/**
 * @author niklaswulf, Christian Bockermann
 * 
 */
public class Map extends MapReduceBase implements
		Mapper<IntWritable, DataWritable, WritableComparable<?>, DataWritable> {
	static Logger log = LoggerFactory.getLogger(Map.class);

	public Map() {
		super("mapper");
	}

	@Override
	public void configure(JobConf conf) {
		super.configure(conf);

		if (processor == null) {
			log.error("This mapper has no processor.");
			return;
		}

		try {
			initMapreduce(processor);
			processor.init(context);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void close() throws IOException {
		log.info("Shutting down mapper.");

		if (processor != null) {
			try {
				processor.finish();
				finishMapreduce(processor);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @see org.apache.hadoop.mapred.Mapper#map(java.lang.Object,
	 *      java.lang.Object, org.apache.hadoop.mapred.OutputCollector,
	 *      org.apache.hadoop.mapred.Reporter)
	 */
	public void map(
			IntWritable id,
			DataWritable data,
			OutputCollector<WritableComparable<?>, DataWritable> outputCollector,
			Reporter reporter) throws IOException {

		context.setOutputCollector(outputCollector);
		context.setReporter(reporter);
		context.setCurrentKey(id);

		log.debug("Processing data #{}: {}", id, data.toString());
		processor.process(data.get());
	}
}
