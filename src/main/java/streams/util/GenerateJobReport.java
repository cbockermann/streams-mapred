package streams.util;

import java.io.IOException;

import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobID;
import org.apache.hadoop.mapred.RunningJob;
import org.apache.hadoop.mapred.TaskAttemptID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.ProcessContext;
import stream.StatefulProcessor;

public class GenerateJobReport implements StatefulProcessor {
	static Logger log = LoggerFactory.getLogger(GenerateJobReport.class);

	public static void main(String[] args) throws IOException {
		String jobName = "attempt_201308071430_0071_m_000006_0";
		if (args.length != 0) {
			jobName = args[0];
		}
		TaskAttemptID taskAttemptID = TaskAttemptID.forName(jobName);
		log.info("" + taskAttemptID);
		JobID jobId = taskAttemptID.getJobID();
		log.info("" + jobId);

		JobClient client = new JobClient();
		RunningJob job = client.getJob(jobId);

		log.info("" + job);

	}

	String longestLine;

	@Override
	public Data process(Data item) {
		String line = (String) item.get("line");
		if (line == null) {
			return null;
		}

		item.put("lineToUpper", item.get("line").toString().toUpperCase());

		if (line.length() > longestLine.length()) {
			longestLine = line;
		}

		return item;
	}

	@Override
	public void finish() throws Exception {
		System.out.println("longestLine = " + longestLine);
	}

	@Override
	public void init(ProcessContext arg0) throws Exception {
		longestLine = "";
	}

	@Override
	public void resetState() throws Exception {
		longestLine = "";
	}
}
