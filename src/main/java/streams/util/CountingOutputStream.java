package streams.util;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class CountingOutputStream extends BufferedOutputStream {

	private int count = 0;

	public CountingOutputStream(OutputStream out) {
		super(out);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		super.write(b, off, len);
		count += len;
	}

	@Override
	public void write(int b) throws IOException {
		super.write(b);
		++count;
	}

	public int getCount() {
		return count;
	}

	public void resetCount() {
		count = 0;
	}

	public int getAndResetCount() {
		int tmp = count;
		count = 0;
		return tmp;
	}
}
